#!/bin/bash -xe
export PATH=/usr/local/bin:$PATH
mkdir /var/log/monitor
mkdir /var/log/task
chown ec2-user:ec2-user /var/log/monitor
chown ec2-user:ec2-user /var/log/task
echo 'log_file="/var/log/task/task.log"' >> /etc/monitor.conf
chown ec2-user:ec2-user /etc/monitor.conf
cat <<EOF > /usr/bin/monitor.sh
#!/bin/bash
log=/var/log/monitor/monitor.log
timestamp()
{
 date +"%Y-%m-%d %H:%M:%S"
}

if [ -n "\$1" ]
then
log_file=\$1
else
. /etc/monitor.conf
fi

while ! test -f "\$log_file"; do
  sleep 5
  echo "\$(timestamp): Still waiting" >> \$log
done

tail -fn0 "\$log_file" | while read line ; do
echo "\${line}" | grep -i "ERROR" > /dev/null
if [ \$? = 0 ] ; then
  echo "\$(timestamp): ERROR found" >> \$log
  echo "\$(timestamp): restart backend@task.service" >> \$log
  sudo systemctl restart backend@task.service
  echo "\$(timestamp): restarted backend@task.service" >> \$log
fi
done
EOF

cat <<EOF > /usr/bin/task.sh
#!/bin/bash
hostname=\$(hostname)
log=/var/log/task/task.log
firsttimestamp()
{
 date +"%b %d %H:%M:%S"
}
secondtimestamp()
{
 date +"%Y-%m-%d %H:%M:%S"
}

echo "\$(firsttimestamp) \$hostname worker-tasks: [\$(secondtimestamp),0: INFO/Started]" >> \$log
for (( i=1; i <= 10; i++ ))
do
sleep 5
echo "\$(firsttimestamp) \$hostname worker-tasks: [\$(secondtimestamp),\$i: INFO/MainProcess]" >> \$log
done
echo "\$(firsttimestamp) \$hostname worker-tasks: [\$(secondtimestamp),11: ERROR/MainProcess] Error in timer: TimeoutError('Timeout reading from socket',). Traceback (most recent call last):" >> \$log
EOF

chown ec2-user:ec2-user /usr/bin/monitor.sh
chown ec2-user:ec2-user /usr/bin/task.sh
chmod +x /usr/bin/monitor.sh
chmod +x /usr/bin/task.sh

cat <<EOF > /lib/systemd/system/backend@monitor.service
[Unit]
Description=Monitor service
After=syslog.target network.target 

[Service]
ExecStart=/usr/bin/monitor.sh

User=ec2-user
Group=ec2-user

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF > /lib/systemd/system/backend@task.service
[Unit]
Description=Task service
After=syslog.target network.target backend@monitor.service

[Service]
ExecStart=/usr/bin/task.sh

User=ec2-user
Group=ec2-user

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload 
systemctl enable backend@monitor.service
systemctl enable backend@task.service
systemctl start backend@monitor.service
systemctl start backend@task.service
