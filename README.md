# ziphy



## Create Cloudformation stack

```aws cloudformation create-stack --stack-name ec2-test --template-body file://instance.yaml --region us-east-1 --parameters ParameterKey=UserData,ParameterValue=$(base64 -i userdata.sh)```

## Delete Cloudformation stack

```aws cloudformation delete-stack --stack-name ec2-test --region us-east-1```


